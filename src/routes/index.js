import React from 'react';
import { Switch } from 'react-router-dom';
import Route from './Route';
import DefaultPage from '../views/pages/DefaultPage/DefaultPage';
import Dashboard from '../views/pages/Dashboard/Dashboard';
import Policy from '../views/pages/Policy/Policy';

export default function Routes() {
  return (
    <Switch>
      <Route path='/' exact component={DefaultPage} />
      <Route path='/Login' exact component={DefaultPage} />
      <Route path='/Policy' exact component={Policy} />
      <Route path='/Dashboard' component={Dashboard} isPrivate />
    </Switch>
  );
}
