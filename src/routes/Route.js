import React from 'react';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';
import AuthLayout from '../views/Layouts/AuthLayout/AuthLayout';
import DeafaultLayout from '../views/Layouts/DeafaultLayout/DeafaultLayout';
// import iff from '../views/common/iff';

export default function RouteWrapper({ component: Component, isPrivate, ...rest }) {
  // getter
  const signed = JSON.parse(localStorage.getItem('isLogged'));
  // console.log('signed', localStorage.getItem('userdata'));

  // const signed = false;

  /**
   * Redirect user to SignIn page if he tries to access a private route
   * without authentication.
   */
  if (isPrivate && !signed) {
    return <Redirect to='/Login' />;
  }

  /**
   * Redirect user to Main page if he tries to access a non private route
   * (SignIn or SignUp) after being authenticated.
   */
  if (!isPrivate && signed) {
    return <Redirect to='/Dashboard' />;
  }

  /**
   * If not included on both previous cases, redirect user to the desired route.
   */
  // return <Route {...rest} component={Component} />;
  const Layout = signed ? AuthLayout : DeafaultLayout;

  /**
   * If not included on both previous cases, redirect user to the desired route.
   */
  return (
    <Route
      {...rest}
      render={(props) => (
        <Layout>
          <Component {...props} />
        </Layout>
      )}
    />
  );
}

RouteWrapper.propTypes = {
  isPrivate: PropTypes.bool,
  component: PropTypes.oneOfType([PropTypes.element, PropTypes.func]).isRequired
};

RouteWrapper.defaultProps = {
  isPrivate: false
};
