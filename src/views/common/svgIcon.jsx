import React from 'react';
import Icon from '@ant-design/icons';

const googleSVG = () => {
  return (
    <svg
      aria-hidden='true'
      focusable='false'
      width='1em'
      height='1em'
      style={{ transform: 'rotate(360deg)' }}
      preserveAspectRatio='xMidYMid meet'
      viewBox='0 0 48 48'>
      <path
        fill='#FFC107'
        d='M43.611 20.083H42V20H24v8h11.303c-1.649 4.657-6.08 8-11.303 8c-6.627 0-12-5.373-12-12s5.373-12 12-12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4C12.955 4 4 12.955 4 24s8.955 20 20 20s20-8.955 20-20c0-1.341-.138-2.65-.389-3.917z'
      />
      <path
        fill='#FF3D00'
        d='M6.306 14.691l6.571 4.819C14.655 15.108 18.961 12 24 12c3.059 0 5.842 1.154 7.961 3.039l5.657-5.657C34.046 6.053 29.268 4 24 4C16.318 4 9.656 8.337 6.306 14.691z'
      />
      <path
        fill='#4CAF50'
        d='M24 44c5.166 0 9.86-1.977 13.409-5.192l-6.19-5.238A11.91 11.91 0 0 1 24 36c-5.202 0-9.619-3.317-11.283-7.946l-6.522 5.025C9.505 39.556 16.227 44 24 44z'
      />
      <path
        fill='#1976D2'
        d='M43.611 20.083H42V20H24v8h11.303a12.04 12.04 0 0 1-4.087 5.571l.003-.002l6.19 5.238C36.971 39.205 44 34 44 24c0-1.341-.138-2.65-.389-3.917z'
      />
    </svg>
  );
};

export const GoogleSVGIcon = (props) => {
  return <Icon component={googleSVG} {...props} />;
};

const facebookSVG = () => {
  return (
    <svg
      aria-hidden='true'
      focusable='false'
      width='1em'
      height='1em'
      style={{ transform: 'rotate(360deg)' }}
      preserveAspectRatio='xMidYMid meet'
      viewBox='0 0 16 16'>
      <path
        fill='#3B5998'
        d='M7.2 16V8.5h-2V5.8h2V3.5C7.2 1.7 8.4 0 11.1 0c1.1 0 1.9.1 1.9.1l-.1 2.5h-1.7c-1 0-1.1.4-1.1 1.2v2H13l-.1 2.7h-2.8V16H7.2z'
      />
    </svg>
  );
};

export const FacebookSVGIcon = (props) => {
  return <Icon component={facebookSVG} {...props} />;
};

const twitterSVG = () => {
  return (
    <svg
      aria-hidden='true'
      focusable='false'
      width='0.75em'
      height='1em'
      style={{ transform: 'rotate(360deg)' }}
      preserveAspectRatio='xMidYMid meet'
      viewBox='0 0 748.681 1000'>
      <path
        d='M748.681 875.015c0 34.315-12.307 63.755-36.922 88.227c-24.615 24.519-54.117 36.758-88.576 36.758H375.078c-103.368 0-191.702-36.519-265.055-109.647C36.65 817.234.001 729.224.001 626.187v-501.08c0-35.311 12.219-64.969 36.649-89.031C61.08 12.049 90.923.001 126.146.001c34.199 0 63.584 12.273 87.981 36.791c24.471 24.492 36.724 53.878 36.724 88.228v181.436h353.233c32.069 0 59.545 11.366 82.466 34.116c22.893 22.71 34.315 49.967 34.315 81.797c0 31.769-11.421 59.073-34.233 81.802c-22.82 22.698-50.239 34.083-82.173 34.083H250.851v87.817c0 34.438 12.021 63.687 36.184 87.817c24.136 24.063 53.441 36.109 87.918 36.109h248.182c34.438 0 64.008 12.287 88.623 36.793c24.615 24.518 36.922 53.925 36.922 88.226'
        fill='#3B5998'
      />
    </svg>
  );
};

export const TwitterSVGIcon = (props) => {
  return <Icon component={twitterSVG} {...props} />;
};

const myProfileSVG = () => {
  return (
    <svg
      aria-hidden='true'
      focusable='false'
      width='3em'
      height='3em'
      style={{ transform: 'rotate(360deg)' }}
      preserveAspectRatio='xMidYMid meet'
      viewBox='0 0 50 50'>
      <path
        d='M25.1 42c-9.4 0-17-7.6-17-17s7.6-17 17-17s17 7.6 17 17s-7.7 17-17 17zm0-32c-8.3 0-15 6.7-15 15s6.7 15 15 15s15-6.7 15-15s-6.8-15-15-15z'
        fill='#ffffff'
      />
      <path
        d='M15.3 37.3l-1.8-.8c.5-1.2 2.1-1.9 3.8-2.7c1.7-.8 3.8-1.7 3.8-2.8v-1.5c-.6-.5-1.6-1.6-1.8-3.2c-.5-.5-1.3-1.4-1.3-2.6c0-.7.3-1.3.5-1.7c-.2-.8-.4-2.3-.4-3.5c0-3.9 2.7-6.5 7-6.5c1.2 0 2.7.3 3.5 1.2c1.9.4 3.5 2.6 3.5 5.3c0 1.7-.3 3.1-.5 3.8c.2.3.4.8.4 1.4c0 1.3-.7 2.2-1.3 2.6c-.2 1.6-1.1 2.6-1.7 3.1V31c0 .9 1.8 1.6 3.4 2.2c1.9.7 3.9 1.5 4.6 3.1l-1.9.7c-.3-.8-1.9-1.4-3.4-1.9c-2.2-.8-4.7-1.7-4.7-4v-2.6l.5-.3s1.2-.8 1.2-2.4v-.7l.6-.3c.1 0 .6-.3.6-1.1c0-.2-.2-.5-.3-.6l-.4-.4l.2-.5s.5-1.6.5-3.6c0-1.9-1.1-3.3-2-3.3h-.6l-.3-.5c0-.4-.7-.8-1.9-.8c-3.1 0-5 1.7-5 4.5c0 1.3.5 3.5.5 3.5l.1.5l-.4.5c-.1 0-.3.3-.3.7c0 .5.6 1.1.9 1.3l.4.3v.5c0 1.5 1.3 2.3 1.3 2.4l.5.3v2.6c0 2.4-2.6 3.6-5 4.6c-1.1.4-2.6 1.1-2.8 1.6z'
        fill='#ffffff'
      />
    </svg>
  );
};
export const MyProfileSVGIcon = (props) => {
  return <Icon component={myProfileSVG} {...props} />;
};
