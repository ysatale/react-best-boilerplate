import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _get from 'lodash.get';
import { Layout } from 'antd';
import { toggleDrawer } from '../../../redux/Actions/Common/drawerAction';
import { AuthLayoutStyle } from './AuthLayoutStyle';
import MainHeader from '../../Components/Header/MainHeader';
import MainDrawer from '../../Components/Drawer/MainDrawer';

const { Footer, Content } = Layout;

const AuthLayout = (props) => {
  return (
    <AuthLayoutStyle>
      <MainHeader {...props} />

      <Layout style={{ background: 'transparent' }}>
        <MainDrawer collapsed={props.collapsed} toggleDrawer={props.toggleDrawer} />
        <Content className='AuthMainContent'>{props.children}</Content>
      </Layout>
      <Footer className='MainFooter'>Syngenta ©2020 Created by Yogesh Satale</Footer>
    </AuthLayoutStyle>
  );
};
const mapDispatchToProps = function (dispatch) {
  return {
    toggleDrawer: () => dispatch(toggleDrawer())
  };
};
const mapStateToProps = function (state) {
  return {
    collapsed: _get(state, 'drawerReducer.collapsed', false)
  };
};

AuthLayout.propTypes = {
  toggleDrawer: PropTypes.func,
  collapsed: PropTypes.bool
};

export default connect(mapStateToProps, mapDispatchToProps)(AuthLayout);
