import styled from 'styled-components';
import { Layout } from 'antd';

export const AuthLayoutStyle = styled(Layout)`
  .AuthMainContent {
    margin: 1.8em 0;
  }
  .MainFooter {
    padding: 10px 50px;
    position: fixed;
    z-index: 1;
    width: 100%;
    bottom: 0;
    color: #fff;
    font-weight: bold;
  }
`;
