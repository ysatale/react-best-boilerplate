import styled from 'styled-components';
import { Layout } from 'antd';
import defaultLayoutbg from '../../../assets/images/default.jpg';

export const DefaultLayoutStyle = styled(Layout)`
  overflow: hidden;
  height: 100vh;
  /* Location of the image */

  background: url(${defaultLayoutbg});

  /* Background image is centered vertically and horizontally at all times */
  background-position: center center;

  /* Background image doesn't tile */
  background-repeat: no-repeat;

  /* Background image is fixed in the viewport so that it doesn't move when 
     the content's height is greater than the image's height */
  background-attachment: fixed;

  /* This is what makes the background image rescale based
     on the container's size */
  background-size: cover;

  /* Set a background color that will be displayed
     while the background image is loading */
  background-color: #464646;

  .Default-MainFooter {
    background: rgba(141, 196, 76, 0.75);
    color: white;
    font-weight: bold;
    padding: 10px;
  }

  /******Media Queries ******/

  @media only screen and (min-width: 801px) {
  }
  @media only screen and (max-width: 800px) {
  }
  @media only screen and (max-width: 550px) {
  }
  @media only screen and (max-width: 500px) {
  }
  @media only screen and (max-width: 360px) {
  }
`;
