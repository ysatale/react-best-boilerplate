import React from 'react';
import { Layout, Typography } from 'antd';
import PropTypes from 'prop-types';

import { DefaultLayoutStyle } from './DefaultLayoutStyle';

const { Title } = Typography;
const { Footer, Content } = Layout;

const DeafaultLayout = (props) => {
  console.log('DeafaultLayout props', props);

  return (
    <DefaultLayoutStyle>
      <Content className='Default-MainConent'>{props.children}</Content>
      <Footer className='Default-MainFooter'>
        <marquee>
          <Title level={4}> YSBLOG ©2020 Created by Yogesh Satale </Title>
        </marquee>
      </Footer>
    </DefaultLayoutStyle>
  );
};

DeafaultLayout.propTypes = {
  ToggleSpinner: PropTypes.func
};

export default DeafaultLayout;
