import styled from 'styled-components';
import { Layout } from 'antd';

const { Sider } = Layout;
export const MainDrawerStyle = styled(Sider)`
  overflow: hidden;
  padding: 0;
  height: 100vh;
`;
