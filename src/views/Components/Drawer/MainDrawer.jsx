import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import _get from 'lodash.get';
import { toggleDrawer } from '../../../redux/Actions/Common/drawerAction';

import { MainDrawerStyle } from './MainDrawerStyle';

const MainDrawer = (props) => {
  return (
    <MainDrawerStyle
      theme='dark'
      collapsible
      collapsedWidth={70}
      collapsed={props.collapsed}
      trigger={null}
    />
  );
};

const mapDispatchToProps = function (dispatch) {
  return {
    toggleDrawer: () => dispatch(toggleDrawer())
  };
};
// export default Drawer;
const mapStateToProps = function (state) {
  console.log('drawer state', state);

  return {
    collapsed: _get(state, 'drawerReducer.collapsed', false)
  };
};
MainDrawer.propTypes = {
  toggleDrawer: PropTypes.func,
  collapsed: PropTypes.bool
};
export default connect(mapStateToProps, mapDispatchToProps)(MainDrawer);
