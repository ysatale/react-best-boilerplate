import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import _get from 'lodash.get';
import { SpinnerStyle } from './SpinnerStyle';

const Spinner = (props) => {
  return <SpinnerStyle spinning={props.loading} tip='Loading...' size='large' />;
};

const mapStateToProps = function (state) {
  console.log('spinner state', state);

  return {
    loading: _get(state, 'spinnerReducer.loading', false)
  };
};

Spinner.propTypes = {
  loading: PropTypes.bool
};
export default connect(mapStateToProps)(Spinner);
