import styled from 'styled-components';
import { Layout } from 'antd';

const { Header } = Layout;

export const MainHeaderStyle = styled(Header)`
  position: fixed;
  z-index: 1;
  width: 100%;
  border-bottom: 2px solid yellow;
  .logo {
    height: 32px;
    margin: 16px;
  }
  .rightMenu {
    float: right;
    text-align: right;
    background: transparent;
    color: #ffffff;
    display: inline-flex;
    cursor: pointer;
    font-size: 14px;
  }
  .rightMenu span {
    padding: 9px 0;
  }
  .rightMenu ul li:hover {
    color: #5c6575;
  }
  .righmenuLink {
    cursor: pointer;
  }
`;
