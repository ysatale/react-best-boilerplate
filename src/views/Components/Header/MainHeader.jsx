import React from 'react';
import PropTypes from 'prop-types';
import isEmpty from 'lodash/isEmpty';

import { Menu, List, Select, Radio, Popover, Button } from 'antd';
import { Link, useHistory, Redirect } from 'react-router-dom';

import { DownCircleFilled, SettingFilled, PoweroffOutlined } from '@ant-design/icons';
import { MyProfileSVGIcon } from '../../common/svgIcon';
import { MainHeaderStyle } from './MainHeaderStyle';
import baseLogo from '../../../assets/images/yslogo.png';
import iff from '../../common/iff';
// const { Meta } = Card;

const MainHeader = (props) => {
  // const [visible, setVisible] = useState(false);
  const history = useHistory();
  const { Option } = Select;

  const handleLogout = () => {
    console.log('clicked logout');
    localStorage.setItem('isLogged', false);
    history.push({
      pathname: '/Login'
    });
    //return <Redirect to="/login" />;
  };
  const Usermenu = (
    <Menu style={{ fontWeight: 500, border: 0 }} className='popupmenuList'>
      <Menu.Item
        style={{ margin: '0px', height: 38, padding: '0 10px' }}
        key='1'
        title='Settings'
        // onClick={() => {
        //   props.history.push(`/settings`);
        //   props.toggleHeader(true);
        // }}
      >
        <SettingFilled />
        <span>Settings</span>
      </Menu.Item>
      <Menu.Item key='2' title='Supported Language' style={{ padding: ' 0 10px' }}>
        <Radio.Group defaultValue='en'>
          <Radio.Button value='en'>EN</Radio.Button>
          <Radio.Button value='hi'>HI</Radio.Button>
          <Radio.Button value='pt'>PT</Radio.Button>
          <Radio.Button value='es'>ES</Radio.Button>
        </Radio.Group>
      </Menu.Item>
      <Menu.Item key='3' title='Logout' onClick={handleLogout} style={{ padding: ' 0 10px' }}>
        <a className='logoutLink'>
          <PoweroffOutlined />
          <span>Logout</span>
        </a>
      </Menu.Item>
    </Menu>
  );

  let countryList = [
    {
      current_region: {
        country_code: 'in',
        flag_Url: 'https://grower-console-assets.s3.ap-south-1.amazonaws.com/flagIcons/flag_in.png'
      }
    }
  ];

  return (
    <MainHeaderStyle>
      <img className='logo' src={baseLogo} />

      <Button onClick={props.toggleDrawer}>Click Me</Button>
      <div className='rightMenu'>
        {iff(
          !isEmpty(countryList) && Object.keys(countryList[0].current_region),
          <Select defaultValue={countryList[0].current_region.country_code} showArrow={false}>
            <Option value={countryList[0].current_region.country_code} style={{ padding: 5 }}>
              <img
                style={{ width: '26px', height: '26px', margin: '0' }}
                src={countryList[0].current_region.flag_Url}
              />
              &nbsp;&nbsp;&nbsp;
              <span style={{ padding: '0', textTransform: 'uppercase' }}>
                {countryList[0].current_region.country_code}
              </span>
            </Option>
          </Select>,
          null
        )}

        <Popover content={Usermenu} className='popoverContent' placement='bottomRight'>
          <MyProfileSVGIcon className='righmenuLink' />
        </Popover>
      </div>
      {/* <Drawer
        title='My Profile'
        placement='right'
        closable={true}
        onClose={this.onClose}
        visible={this.state.visible}
      /> */}
    </MainHeaderStyle>
  );
};

MainHeader.propTypes = {
  toggleDrawer: PropTypes.func,
  collapsed: PropTypes.bool
};
export default MainHeader;
