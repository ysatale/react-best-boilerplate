import React, { useState } from 'react';
// import PropTypes from 'prop-types';
// import { connect } from 'react-redux';
// import { ToggleSpin } from '../../../redux/Actions/Common/spinnerAction';
import { DefaultPageStyle } from './DefaultPageStyle';
import LoginForm from '../../Forms/Login/LoginForm';
import SignUpForm from '../../Forms/SignUp/SignUpForm';
import ForgotForm from '../../Forms/Forgot/ForgotForm';
import iff from '../../common/iff';

const DefaultPage = (props) => {
  console.log('defaultPage', props);

  const [login, setLogin] = useState(true);
  const [signup, setSignup] = useState(false);
  const [forgot, setForgot] = useState(false);

  return (
    <DefaultPageStyle
      title={iff(login || signup, iff(signup, 'Sign Up', 'Sign In'), 'Forgot Password')}
      bordered={false}>
      {iff(
        login || signup,
        iff(
          signup,
          <SignUpForm setLogin={setLogin} setSignup={setSignup} setForgot={setForgot} />,
          <LoginForm
            // ToggleSpin={props.ToggleSpin}
            setLogin={setLogin}
            setSignup={setSignup}
            setForgot={setForgot}
          />
        ),
        <ForgotForm setLogin={setLogin} setSignup={setSignup} setForgot={setForgot} />
      )}
    </DefaultPageStyle>
  );
};

// const mapDispatchToProps = function (dispatch) {
//   return { ToggleSpin: () => dispatch(ToggleSpin()) };
// };
// const mapStateToProps = function () {
//   return {};
// };

// DefaultPage.propTypes = {
//   ToggleSpin: PropTypes.func
// };

// export default connect(mapStateToProps, mapDispatchToProps)(DefaultPage);
export default DefaultPage;
