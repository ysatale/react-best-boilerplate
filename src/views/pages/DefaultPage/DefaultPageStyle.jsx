import styled from 'styled-components';
import { Card } from 'antd';

export const DefaultPageStyle = styled(Card)`
  width: 350px;
  margin: 1em auto;
  background: #ffe047bd;
  box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.25);

  /*******AntD related classes  */
  .ant-card-head {
    text-align: center;
    font-weight: bold;
  }
  .ant-card-head-title {
    padding: 10px;
  }
  .ant-card-body {
    padding: 20px;
  }

  /******Media Queries ******/

  @media only screen and (min-width: 1500px) {
  }
  @media only screen and (min-width: 801px) {
  }
  @media only screen and (max-width: 800px) {
  }
  @media only screen and (max-width: 550px) {
  }
  @media only screen and (max-width: 500px) {
  }
  @media only screen and (max-width: 360px) {
  }
`;
