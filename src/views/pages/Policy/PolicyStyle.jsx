import { Card } from 'antd';
import styled from 'styled-components';

export const PolicyStyle = styled(Card)`
  width: 50em;
  height: 87vh;
  overflow-y: auto;
  overflow-x: hidden;
  margin: 1em auto;
  background: #ffe047bd;
  box-shadow: inset 0 0 10px rgba(0, 0, 0, 0.25);
`;
