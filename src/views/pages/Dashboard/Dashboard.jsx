import React from 'react';
import { Col, Row, Card } from 'antd';
import { DashboardStyle, CardWrapper } from './DashboardStyle';

const Dashboard = () => {
  const Rowdata = [
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' },
    { id: 1, title: 'Card one' }
  ];
  return (
    <DashboardStyle>
      <CardWrapper title={`Property (${0})`}>
        <Row gutter={16} className='cardScrollar'>
          {Rowdata.map((item, index) => {
            return (
              <Col key={index} span={16}>
                <Card className='propertyCard' title={item.id} bordered={false}>
                  {item.title}
                </Card>
              </Col>
            );
          })}
        </Row>
      </CardWrapper>
      <CardWrapper title={`Seasons (${0})`}>
        <Row gutter={16} className='cardScrollar'>
          {Rowdata.map((item, index) => {
            return (
              <Col key={index} span={16}>
                <Card className='propertyCard' title={item.id} bordered={false}>
                  {item.title}
                </Card>
              </Col>
            );
          })}
        </Row>
      </CardWrapper>
    </DashboardStyle>
  );
};

export default Dashboard;
