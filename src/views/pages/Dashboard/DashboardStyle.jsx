import styled from 'styled-components';
import { Layout, Card } from 'antd';

const { Content } = Layout;
export const DashboardStyle = styled(Content)`
  .cardScrollar {
    height: 13em;
    width: 93.5em;
    overflow: auto hidden;
    flex-direction: row;
    display: -webkit-inline-box;
    white-space: nowrap;
  }
  .propertyCard {
    background-color: red;
  }
`;
export const CardWrapper = styled(Card)`
  margin: 10px 0;
  border-radius: 8px;
  .ant-card-head-title {
    padding: 10px 0;
  }
  .ant-card-body {
    padding: 10px 24px;
  }
`;
