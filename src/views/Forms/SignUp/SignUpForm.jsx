import React, { useEffect } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Divider, Typography, Row, Col } from 'antd';
import { UserOutlined, MobileOutlined, MailOutlined } from '@ant-design/icons';
import { SignUpFormStyle } from './SignUpFormStyle';
import { GoogleSVGIcon, FacebookSVGIcon, TwitterSVGIcon } from '../../common/svgIcon';
import InputMobileWithCountry from '../components/InputMobileWithCountry/InputMobileWithCountry';

const SignUpForm = (props) => {
  const [form] = Form.useForm();
  const { Text } = Typography;
  const { setLogin, setSignup, setForgot } = props;

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
  };

  const onFinishFailed = (errors) => {
    console.log('signup onsubmit error', errors);
  };

  return (
    <SignUpFormStyle
      name='normal_signup'
      className='signup-form'
      initialValues={{
        Fname: null,
        mobile: '',
        email: '',
        ['TermsCheck']: ['terms'],
        ['UpdatesCheck']: ['notify']
      }}
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}
      size='middle'>
      <Form.Item name='Fname' rules={[{ required: true, message: 'Please input your Full Name!' }]}>
        <Input prefix={<UserOutlined className='site-form-item-icon' />} placeholder='Full Name' />
      </Form.Item>
      <InputMobileWithCountry initCountryCode='in' Mobname='mobile' />

      <Form.Item
        name='email'
        rules={[
          { required: true, message: 'Please input your Email!' },
          {
            type: 'email',
            message: 'The input is not valid E-mail!'
          }
        ]}>
        <Input prefix={<MailOutlined className='site-form-item-icon' />} placeholder='Email' />
      </Form.Item>

      {/* <Form.Item
        name='password'
        rules={[{ required: true, message: 'Please input your Password!' }]}>
        <Input
          prefix={<LockOutlined className='site-form-item-icon' />}
          type='password'
          placeholder='Password'
        />
      </Form.Item> */}
      <Form.Item
        rules={[{ required: true, message: 'Please accept Terms and Conditions' }]}
        name='TermsCheck'>
        <Checkbox.Group>
          <Checkbox value='terms'>
            Accept
            <Link style={{ padding: '0 10px' }} to='/Policy'>
              Terms And Conditions
            </Link>
          </Checkbox>
        </Checkbox.Group>
      </Form.Item>
      <Form.Item name='UpdatesCheck'>
        <Checkbox.Group>
          <Checkbox style={{ margin: 0 }} value='notify'>
            I&apos;d like to receive updates via email.
          </Checkbox>
        </Checkbox.Group>
      </Form.Item>

      <Form.Item>
        <Button block type='primary' htmlType='submit' className='signup-form-submit'>
          Submit
        </Button>
      </Form.Item>
      <Form.Item style={{ margin: 0 }}>
        <Text style={{ lineHeight: '30px' }}>Already have account</Text>
        <Button
          type='link'
          className='signup-form-loginLink'
          onClick={() => {
            setSignup(false);
            setForgot(false);
            setLogin(true);
          }}>
          Sign In
        </Button>
      </Form.Item>
      <Divider dashed className='signup-form-divider'>
        OR
      </Divider>
      <span>Sign up with</span>
      <Form.Item style={{ margin: 0 }}>
        <Row gutter={16}>
          <Col span={8}>
            <Button className='signup-form-button'>
              <GoogleSVGIcon />
              Google
            </Button>
          </Col>
          <Col span={8}>
            <Button className='signup-form-FBbutton'>
              <FacebookSVGIcon />
              Facebook
            </Button>
          </Col>
          <Col span={8}>
            <Button className='signup-form-button'>
              <TwitterSVGIcon />
              Twitter
            </Button>
          </Col>
        </Row>
      </Form.Item>
    </SignUpFormStyle>
  );
};

SignUpForm.propTypes = {
  setSignup: PropTypes.func,
  setForgot: PropTypes.func,
  setLogin: PropTypes.func
};

export default SignUpForm;
