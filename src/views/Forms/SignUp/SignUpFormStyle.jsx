import { Form } from 'antd';
import styled from 'styled-components';

export const SignUpFormStyle = styled(Form)`
  .signup-form-loginLink {
    float: right;
    /* font-size: 16px; */
    font-weight: bold;
    padding: 0;
  }
  .signup-form-divider {
    margin: 0;
  }
  .signup-form-FBbutton {
    /* margin: 0 5px; */
    padding: 4px 8px;
  }
  .ant-form-item-control-input {
    min-height: 20px;
  }
  /******Media Queries ******/

  @media only screen and (min-width: 801px) {
  }
  @media only screen and (max-width: 800px) {
  }
  @media only screen and (max-width: 550px) {
  }
  @media only screen and (max-width: 500px) {
  }
  @media only screen and (max-width: 360px) {
  }
`;
