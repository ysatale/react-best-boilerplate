import React, { useState } from 'react';
import { Upload, Button, message } from 'antd';
import { LoadingOutlined, PlusOutlined } from '@ant-design/icons';
import get from 'lodash/get';

export const Fileupload = (props) => {
  const [imageUrl, setimageUrl] = useState();
  const [loading, setLoading] = useState(false);

  const uploadButton = (
    <div>
      {loading ? <LoadingOutlined /> : <PlusOutlined />}
      <div className='ant-upload-text'>Upload</div>
    </div>
  );
  const getBase64 = (file) => {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();
      reader.readAsDataURL(file);
      reader.onload = () => resolve(reader.result);
      reader.onerror = (error) => reject(error);
    });
  };
  const handleChange = async (info) => {
    if (get(info, 'file.originFileObj', null)) {
      setLoading(true);
      if (!info.file.url) {
        info.file.preview = await getBase64(info.file.originFileObj);
      }
      setimageUrl(info.file.preview);
      props.setOrgImage(info.file.preview);
      setLoading(false);
      // props.upload(get(info, "file.originFileObj", null));
    }
  };
  return (
    <Upload
      listType='picture-card'
      className='avatar-uploader'
      showUploadList={false}
      onChange={handleChange}>
      {imageUrl ? <img src={imageUrl} alt='avatar' style={{ width: '100%' }} /> : uploadButton}
    </Upload>
  );
};
