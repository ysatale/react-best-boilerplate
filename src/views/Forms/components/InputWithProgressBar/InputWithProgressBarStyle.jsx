import styled from 'styled-components';
import { Form, Progress } from 'antd';

export const ProgressBar = styled(Progress)`
  .ant-progress-inner {
    width: 313px;
  }
`;
export const PwdStrenthStyle = styled.div`
  padding-left: 11em;
  align-self: initial;
  text-align: right;
`;
export const InputWithProgressStyle = styled(Form.Item)`
  text-align: left;
  /* width:20em;  */
  .password-strength-meter-progress {
    -webkit-appearance: none;
    appearance: none;
    /* width: 250px; */
    height: 5px;
  }

  .password-strength-meter-progress::-webkit-progress-bar {
    background-color: #eee;
    border-radius: 3px;
  }

  .password-strength-meter-label {
    float: right;
  }

  .password-strength-meter-progress::-webkit-progress-value {
    border-radius: 2px;
    background-size: 35px 20px, 100% 100%, 100% 100%;
  }
  .ant-form-vertical .ant-form-item-label {
    padding: 0;
  }
  .ant-progress-inner {
    height: 5px;
  }
  .ant-form-item-label > label {
    width: 100%;
  }
`;
