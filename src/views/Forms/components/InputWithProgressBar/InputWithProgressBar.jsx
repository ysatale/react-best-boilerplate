import React, { useState } from 'react';
import zxcvbn from 'zxcvbn';
import { Input } from 'antd';
import { ProgressBar, PwdStrenthStyle, InputWithProgressStyle } from './InputWithProgressBarStyle';

export const InputWithProgressBar = (props) => {
  const [password, setPwdState] = useState('');

  const testedResult = zxcvbn(password);
  console.log('testedResult', testedResult);
  const createPasswordLabel = (result) => {
    switch (result.score) {
      case 0:
        return 'Weak';
      case 1:
        return 'Weak';
      case 2:
        return 'Medium';
      case 3:
        return 'Strong';
      case 4:
        return 'Very Strong';
      default:
        return 'Weak';
    }
  };
  const createProgressColor = (result) => {
    switch (result.score) {
      case 0:
        return '#eb8205';
      case 1:
        return '#eb8205';
      case 2:
        return '#f2cd08';
      case 3:
        return '#749204';
      case 4:
        return '#749204';
      default:
        return '#eb8205';
    }
  };

  const createProgressPercent = (result) => {
    switch (result.score) {
      case 0:
        return 0;
      case 1:
        return 20;
      case 2:
        return 40;
      case 3:
        return 80;
      case 4:
        return 100;
      default:
        return 0;
    }
  };
  const validatePassword = (rule, value) => {
    console.log('Password', typeof value);
    console.log('testedResult', testedResult.score);
    const pattern = /^.*(?=.{8,})((?=.*[!@#$%^&*()-_=+{};:,<.>]){1})(?=.*d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/;

    // if (value != undefined && pattern.test(value) == false) {
    //   props.setValidPwdField(false);
    //   return Promise.reject("Password must be valid");
    // } else {
    //   props.setValidPwdField(true);
    //   return Promise.resolve();
    // }

    if (testedResult.score > 2 && testedResult.score < 3) {
      props.setValidPwdField(false);
      return Promise.reject('Password must be valid');
    }
    props.setValidPwdField(true);
    return Promise.resolve();
  };
  return (
    <>
      <InputWithProgressStyle
        label={
          <>
            <div>Password</div>
            {password && (
              <PwdStrenthStyle>
                <strong
                  style={{
                    color: `${createProgressColor(testedResult)}`
                  }}>
                  {createPasswordLabel(testedResult)}
                </strong>
              </PwdStrenthStyle>
            )}
          </>
        }
        name='password'
        rules={[
          { required: true, message: 'Please enter your password' },
          { validator: validatePassword }
        ]}>
        <Input.Password
          size='large'
          autoComplete='off'
          onChange={(e) => setPwdState(e.target.value)}
        />
      </InputWithProgressStyle>

      <ProgressBar
        size='small'
        percent={createProgressPercent(testedResult)}
        showInfo={false}
        strokeColor={createProgressColor(testedResult)}
      />
    </>
  );
};

export default InputWithProgressBar;
