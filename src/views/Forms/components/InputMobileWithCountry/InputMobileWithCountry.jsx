import React, { useState } from 'react';
import PropTypes from 'prop-types';
import PhoneInput from 'react-phone-input-2';
import 'react-phone-input-2/lib/style.css';
// import ar from 'react-phone-input-2/lang/ar.json';
import { Form } from 'antd';
// import { PhoneInputStyle } from './InputMobileWithCountryStyle';

const InputMobileWithCountry = (props) => {
  const [phone, setPhone] = useState(props.initCountryCode);

  const checkMobile = (rule, value) => {
    if (value && value.replace(/[^0-9]+/g, '').slice(' ').length < 11) {
      return Promise.reject('Invalid mobile number');
    }
    return Promise.resolve();
  };
  return (
    <Form.Item
      name={props.Mobname}
      rules={[
        { required: true, message: 'Please enter mobile number' },
        { validator: checkMobile }
      ]}>
      <PhoneInput
        // localization={ar}
        inputClass='ant-form-item-has-error ant-input-affix-wrapper'
        inputStyle={{ width: '310px' }}
        country={phone}
        value={phone}
        onChange={(val) => setPhone(val)}
      />
    </Form.Item>
  );
};

InputMobileWithCountry.propTypes = {
  Mobname: PropTypes.string,
  initCountryCode: PropTypes.string
};
export default InputMobileWithCountry;
