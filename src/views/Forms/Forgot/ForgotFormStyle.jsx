import styled from 'styled-components';
import { Form } from 'antd';

export const ForgotFormStyle = styled(Form)`
  .forgot-form-loginLink {
    float: right;
    font-weight: bold;
    padding: 0;
  }

  /******Media Queries ******/

  @media only screen and (min-width: 801px) {
  }
  @media only screen and (max-width: 800px) {
  }
  @media only screen and (max-width: 550px) {
  }
  @media only screen and (max-width: 500px) {
  }
  @media only screen and (max-width: 360px) {
  }
`;
