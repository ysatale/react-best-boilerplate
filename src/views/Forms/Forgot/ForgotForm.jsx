import React from 'react';
import PropTypes from 'prop-types';

import { Form, Input, Button, Typography } from 'antd';
import { MailOutlined } from '@ant-design/icons';
import { ForgotFormStyle } from './ForgotFormStyle';

const ForgotForm = (props) => {
  const { Text } = Typography;

  const onFinish = (values) => {
    console.log('Success:', values);
  };

  const onFinishFailed = (errorInfo) => {
    console.log('Failed:', errorInfo);
  };

  return (
    <ForgotFormStyle
      name='forgot_form'
      className='forgot-form'
      initialValues={{ email: '' }}
      size='middle'
      onFinish={onFinish}
      onFinishFailed={onFinishFailed}>
      <Form.Item
        name='email'
        rules={[
          { required: true, message: 'Please input your Email!' },
          {
            type: 'email',
            message: 'The input is not valid E-mail!'
          }
        ]}>
        <Input prefix={<MailOutlined className='site-form-item-icon' />} placeholder='Email' />
      </Form.Item>
      <Form.Item>
        <Button block type='primary' htmlType='submit' className='forgot-form-submit'>
          Send Mail
        </Button>
      </Form.Item>
      <Form.Item style={{ margin: 0 }}>
        <Text style={{ lineHeight: '30px' }}>You can also go back</Text>
        <Button
          type='link'
          className='forgot-form-loginLink'
          onClick={() => {
            props.setForgot(false);
            props.setSignup(false);
            props.setLogin(true);
          }}>
          Login now
        </Button>
      </Form.Item>
    </ForgotFormStyle>
  );
};

ForgotForm.propTypes = {
  setSignup: PropTypes.func,
  setForgot: PropTypes.func,
  setLogin: PropTypes.func
};

export default ForgotForm;
