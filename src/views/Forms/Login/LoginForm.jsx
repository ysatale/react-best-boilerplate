import React, { useState } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { ToggleSpin } from '../../../redux/Actions/Common/spinnerAction';
import { Link, useHistory, Redirect } from 'react-router-dom';
import { Form, Input, Button, Checkbox, Divider, Radio, Typography, Row, Col } from 'antd';
import { LockOutlined, MailOutlined } from '@ant-design/icons';
import { GoogleSVGIcon, FacebookSVGIcon, TwitterSVGIcon } from '../../common/svgIcon';
import iff from '../../common/iff';
import { LoginFormStyle } from './LoginFormStyle';
import InputMobileWithCountry from '../components/InputMobileWithCountry/InputMobileWithCountry';

const LoginForm = (props) => {
  console.log('login props', props);
  const { Text } = Typography;

  const [mobOpt, setmobOpt] = useState(true);
  const [LoginOpt, setLoginOption] = useState('mobileopt');
  const history = useHistory();

  const onFinish = (values) => {
    console.log('Received values of form: ', values);
    props.ToggleSpin();
    setTimeout(() => {
      localStorage.setItem('isLogged', true);
      localStorage.setItem('userdata', JSON.stringify(values));
      if (localStorage.getItem('isLogged')) {
        props.ToggleSpin();
        history.push({
          pathname: '/Dashboard'
        });
      }
    }, 10000);
  };
  const getOption = (e) => {
    if (e.target.value === 'mobileopt') {
      setLoginOption('mobileopt');
      setmobOpt(true);
    } else {
      setLoginOption('emailopt');
      setmobOpt(false);
    }
  };
  const ForgotCall = () => {
    props.setSignup(false);
    props.setLogin(false);
    props.setForgot(true);
  };
  const RegCall = () => {
    props.setForgot(false);
    props.setLogin(false);
    props.setSignup(true);
  };
  const validatePassword = (rule, value) => {
    const PwdPatt = new RegExp(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/
    );
    if (value && !PwdPatt.test(value)) {
      return Promise.reject('Invalid Password');
    }
    return Promise.resolve();
  };
  return (
    <LoginFormStyle
      name='normal_login'
      className='login-form'
      initialValues={{ remember: true, loginOption: 'mobileopt' }}
      onFinish={onFinish}
      size='middle'>
      <Form.Item style={{ textAlign: 'center' }} name='loginOption'>
        <Radio.Group value={LoginOpt} onChange={getOption}>
          <Radio.Button value='mobileopt'>Mobile</Radio.Button>
          <Radio.Button value='emailopt'>Email</Radio.Button>
        </Radio.Group>
      </Form.Item>
      {iff(
        mobOpt,
        <InputMobileWithCountry initCountryCode='in' Mobname='mobile' />,
        <>
          <Form.Item
            name='email'
            rules={[
              { required: true, message: 'Please input your Email!' },
              {
                type: 'email',
                message: 'The input is not valid E-mail!'
              }
            ]}>
            <Input prefix={<MailOutlined className='site-form-item-icon' />} placeholder='Email' />
          </Form.Item>
          <Form.Item
            name='password'
            rules={[
              { required: true, message: 'Please input your Password!' },
              { validator: validatePassword }
            ]}>
            <Input.Password
              prefix={<LockOutlined className='site-form-item-icon' />}
              type='password'
              placeholder='Password'
            />
          </Form.Item>
          <Form.Item>
            <Form.Item name='remember' valuePropName='checked' noStyle>
              <Checkbox className='login-form-remember'>Remember me</Checkbox>
            </Form.Item>

            <Button type='link' className='login-form-forgot' onClick={ForgotCall}>
              Forgot password
            </Button>
          </Form.Item>
        </>
      )}

      <Form.Item>
        <Button block type='primary' htmlType='submit' className='login-form-submit'>
          Log in
        </Button>
      </Form.Item>
      <Form.Item style={{ margin: 0 }}>
        <Text style={{ lineHeight: '30px' }}> Don&apos;t have an account yet?</Text>
        <Button type='link' className='login-form-regLink' onClick={RegCall}>
          Register now
        </Button>
      </Form.Item>
      <Divider dashed className='login-form-divider'>
        OR
      </Divider>
      <Text>Sign in with</Text>
      <Form.Item style={{ margin: 0 }}>
        <Row gutter={16}>
          <Col span={8}>
            <Button className='login-form-button'>
              <GoogleSVGIcon />
              Google
            </Button>
          </Col>
          <Col span={8}>
            <Button className='login-form-FBbutton'>
              <FacebookSVGIcon />
              Facebook
            </Button>
          </Col>
          <Col span={8}>
            <Button className='login-form-button'>
              <TwitterSVGIcon />
              Twitter
            </Button>
          </Col>
        </Row>
      </Form.Item>
    </LoginFormStyle>
  );
};

const mapDispatchToProps = function (dispatch) {
  return { ToggleSpin: () => dispatch(ToggleSpin()) };
};
const mapStateToProps = function () {
  return {};
};

LoginForm.propTypes = {
  setSignup: PropTypes.func,
  setForgot: PropTypes.func,
  setLogin: PropTypes.func,
  ToggleSpin: PropTypes.func
};

export default connect(mapStateToProps, mapDispatchToProps)(LoginForm);

// export default LoginForm;
