import { Form } from 'antd';
import styled from 'styled-components';

export const LoginFormStyle = styled(Form)`
  .login-form-forgot {
    float: right;
    font-weight: bold;
    padding: 0;
  }
  .login-form-remember {
    font-weight: bold;
    padding: 4px 0;
  }
  .login-form-regLink {
    float: right;
    font-weight: bold;
    padding: 0;
  }
  .login-form-divider {
    margin: 0;
  }
  .login-form-FBbutton {
    /* margin: 0 5px; */
    padding: 4px 8px;
  }

  /******Media Queries ******/

  @media only screen and (min-width: 801px) {
  }
  @media only screen and (max-width: 800px) {
  }
  @media only screen and (max-width: 550px) {
  }
  @media only screen and (max-width: 500px) {
  }
  @media only screen and (max-width: 360px) {
  }
`;
