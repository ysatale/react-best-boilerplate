import { createGlobalStyle } from 'styled-components';

export default createGlobalStyle`
  
  * {
    margin: 0;
    padding: 0;
    outline: 0; 
    border: 0;
    box-sizing: border-box;
  }
  *:focus {
    outline: 0;
  }
  html, body, #root {
    height: 100%;
  }
  ul {
    list-style: none;
  }
  button {
    cursor: pointer;
  }
  ::-webkit-scrollbar {
    width: 6px;
    height: 5px;
    cursor: pointer;
  }

  ::-webkit-scrollbar-track {
    cursor: pointer;
    border-radius: 5px;
    background: #c1c5c8;
    border-top: 2px solid white;
    border-bottom: 2px solid white;
    border-right: 2px solid white;
    border-left: 2px solid white;
  }

  ::-webkit-scrollbar-thumb {
    cursor: pointer;
    border-radius: 5px;
    -webkit-box-shadow: inset 0 3px 2px 6px #00B5D8;
  }

  .ant-popover-inner-content {
    padding: 0;
  }
    /******Media Queries ******/

  @media only screen and (min-width: 801px) {
    body {
      background: #fff;
      font-size: 20px;
      /* height: 200em; */
    }
  }
  @media only screen and (max-width: 800px) {
    body {
      background: #fff;
      overflow: hidden;
      font-size: 20px;
      height: 200em;
    }
  }
  @media only screen and (max-width: 550px) {
    body {
      background: #fff;
      overflow: hidden;
      font-size: 30px;
      height: 200em;
    }
  }
  @media only screen and (max-width: 500px) {
    body {
      background: #fff;
      overflow: hidden;
      font-size: 30px;
      height: 200em;
    }
  }

  @media only screen and (max-width: 360px) {
    body {
      background: #fff;
      overflow: hidden;
      font-size: 30px;
      height: 200em;
    }
  }

`;
