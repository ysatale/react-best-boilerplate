import i18n from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import loopObject from 'loopobject';
import { initReactI18next } from 'react-i18next';
import translations from './translations.json';

const resources = {};

loopObject(translations, (data, key) => {
  if (resources.en === undefined) {
    resources.en = {};
  }
  if (resources.en.translation === undefined) {
    resources.en.translation = {};
  }
  resources.en.translation[key] = key;
  loopObject(data, (str, lang) => {
    if (resources[lang] === undefined) {
      resources[lang] = {};
    }
    if (resources[lang].translation === undefined) {
      resources[lang].translation = {};
    }
    resources[lang].translation[key] = str;
  });
});
console.log(resources);
i18n
  .use(LanguageDetector)
  .use(initReactI18next)
  .init({
    // we init with resources
    resources,
    lng: localStorage.getItem('i18nextLng') || 'en',
    // lng: 'hi',
    fallbackLng: {
      'en-GB': ['en'],
      'en-US': ['en'],
      'fr-FR': ['fr'],
      default: ['en']
    },
    debug: process.env.node_env !== 'production',
    keySeparator: false, // we use content as keys
    interpolation: {
      escapeValue: false
    }
  });

export default i18n;
