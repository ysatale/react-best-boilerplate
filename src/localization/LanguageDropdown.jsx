import React, { Component } from 'react';
import { Menu, Dropdown, Icon } from 'antd';
// import styled from 'styled-components';
import i18n from './i18';

// const Container = styled(Dropdown)`
//   color: #8e9293;
// `;

const langs = [
  { key: 'en', label: 'English (USA)' },
  { key: 'hi', label: 'Hindi' },
  { key: 'pt', label: 'Portuguese' }
];
export default class LanguageDropdown extends Component {
  constructor(props) {
    super(props);
    this.state = {
      selected: localStorage.getItem('i18nextLng') || 'en'
    };
    this.onClick = this.onClick.bind(this);
    // TODO - we need to use class properties.
  }

  getSelectedLangLabel() {
    const s = langs.filter((v) => {
      console.log('this.state.selected', this.state.selected);
      return v.key === this.state.selected;
    });
    if (s.length !== 0) {
      return s[0].label;
    }
    return undefined;
  }

  onClick({ key }) {
    this.setState({
      selected: key
    });
    i18n.changeLanguage(key);
  }

  render() {
    const menu = (
      <Menu onClick={this.onClick}>
        {langs.map((v) => {
          return <Menu.Item key={v.key}>{v.label}</Menu.Item>;
        })}
      </Menu>
    );
    return (
      <div style={{ textAlign: 'right' }}>
        <Dropdown overlay={menu}>
          <a
            style={{ color: '#8E9293' }}
            className='ant-dropdown-link'
            onClick={(e) => e.preventDefault()}>
            {this.getSelectedLangLabel()}
            <Icon type='down' />
          </a>
        </Dropdown>
      </div>
    );
  }
}
