import { TOGGLE_SPIN_STATUS } from '../../Constants/index';

export const ToggleSpin = () => {
  return {
    type: TOGGLE_SPIN_STATUS
  };
};
