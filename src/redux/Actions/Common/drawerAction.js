import { TOGGLE_DRAWER_STATUS } from '../../Constants/index';

export const toggleDrawer = () => {
  return {
    type: TOGGLE_DRAWER_STATUS
  };
};
