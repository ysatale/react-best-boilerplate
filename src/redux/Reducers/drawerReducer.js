import { TOGGLE_DRAWER_STATUS } from '../Constants/index';

const initialState = {
  collapsed: false
};

function drawerReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_DRAWER_STATUS:
      return {
        collapsed: !state.collapsed
      };

    default:
      return state;
  }
}
export default drawerReducer;
