const initialState = {
  user: {}
};

function authReducer(state = initialState, action) {
  switch (action.type) {
    case 'SET_USER_INFO':
      return {
        user: action.payload
      };
    default:
      return state;
  }
}

export default authReducer;
