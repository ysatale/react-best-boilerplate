import { TOGGLE_SPIN_STATUS } from '../Constants/index';

const initialState = {
  loading: false
};

function spinnerReducer(state = initialState, action) {
  switch (action.type) {
    case TOGGLE_SPIN_STATUS:
      return {
        loading: !state.loading
      };

    default:
      return state;
  }
}
export default spinnerReducer;
