import { combineReducers } from 'redux';
import authReducer from '../Reducers/authReducer';
import spinnerReducer from '../Reducers/spinnerReducer';
import drawerReducer from '../Reducers/drawerReducer';

export default combineReducers({ authReducer, spinnerReducer, drawerReducer });
