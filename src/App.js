import React from 'react';
import { Provider } from 'react-redux';
import { Router } from 'react-router-dom';
import { store } from './store';
import history from './services/history';
import Routes from './routes';
import GlobalStyles from './views/GlobalStyle/globalStyle';
import Spinner from './views/Components/Spiner/Spinner';

function App(props) {
  return (
    <Provider store={store}>
      <Router history={history}>
        <Spinner />
        <Routes {...props} />
        <GlobalStyles />
      </Router>
    </Provider>
  );
}

export default App;
